require_relative 'fasta'
require 'ruby-progressbar'
require 'logger'

class Database

  def open_seq_files(query_file, data_file)
    @query = fasta_parse(query_file)
    @data = fasta_parse(data_file)
  end

  def set_parameters(log_filepath, ktup, best_diag_count, matrix_file, gap, width)
    @log_filepath = log_filepath
    @ktup = ktup
    @best_diag_count = best_diag_count
    @gap = gap
    @width = width
    init_matrix(matrix_file)
  end

  def do_fasta
    File.delete(@log_filepath)
    result_list = Array.new
    logger = Logger.new @log_filepath
    logger.level = Logger::INFO
    logger.formatter = proc do|severity, datetime, progname, msg|
      "#{msg}\n"
    end
    logger << "FastA algorithm\n"
    logger << "Parameters: Ktup: #{@ktup}, best_diag_count: #{@best_diag_count}, gap: #{@gap}, width: #{@width}"
    logger << "Processing for #{@query.length} query sequences and #{@data.length} query sequences\n"
    puts 'Starting algorithm'
    progress_bar = ProgressBar.create(:title => 'FastA', :total => @data.length, :format => '%t: <%B> %a %e %P%')
    logger << "*"*100
    for query_seq in @query
      query_seq.build_lookup_table
      for data_seq in @data
        data_seq.build_lookup_table
        fasta_algorithm = Fasta.new(query_seq, data_seq)
        fasta_algorithm.set_parameters(@ktup, @best_diag_count, @gap, @width)
        seq1, seq2, score = fasta_algorithm.algorithm
        current_alignment = Alignment.new(query_seq, data_seq, score, seq1, seq2)
        result_list.push(current_alignment)
        progress_bar.increment
        # puts "\n"
      end
    end
    result_list.sort! {|x, y| y.score <=> x.score }
    result_list.each do |alignment|
      logger << "\n"*2
      logger << "#"*100
      logger.info "\nQuery seq: #{alignment.query_seq.header}\n#{alignment.query_seq.seq}"
      logger.info "\nData seq: #{alignment.data_seq.header}\n#{alignment.data_seq.seq}"
      logger.info "Finished algorithm with score #{alignment.score}"
      logger.info "Query alignment:"
      logger.info alignment.query_alignment
      logger.info "Data alignment"
      logger.info alignment.data_alignment

    end
  end

  def get_data
    @data
  end

  def get_query
    @query
  end

  private

  def fasta_parse(filepath)
    sequences = Array.new
    header = seq = ''
    File.open(filepath).each_line do |line|
      if line[0] == '>'
        sequences.push(Sequence.new(header, seq)) unless header.empty?
        header = line[1..-1].lstrip.strip
        seq = ''
      else
        seq << line.lstrip.strip
      end
    end
    sequences.push(Sequence.new(header, seq)) unless header.empty?
    return sequences
  end

  def init_matrix(filepath)

    file = File.open(filepath)
    data = file.read
    data = data.split("\n").map{|l| l.chomp.split}
    row0 = data.shift

    $subs_matrix['-'] = {}
    $subs_matrix['-'].default = @gap
    data.each{|row|
      x = row.shift
      $subs_matrix[x] = {}
      $subs_matrix[x].default = @gap
      row0.zip(row).each{|y,s|
        $subs_matrix['-'][y] = @gap
        $subs_matrix[x][y] = s.to_i
        $subs_matrix[x][x] = 1 if $subs_matrix[x][x].nil?
      }
    }

    $subs_matrix.default = $subs_matrix['-']

  end
end