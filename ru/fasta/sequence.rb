
class Sequence

  attr_reader :header,
              :seq,
              :lookup_table


  def initialize(header, seq)
    @header = header
    @seq = seq.delete('\n')

  end

  def build_lookup_table
    @lookup_table = Hash.new
    seq.each_char { |letter|
      temp = Array.new
      (0...seq.length).each { |i| temp.push(i) if seq[i] == letter }
      @lookup_table[letter] = temp
    }
  end

end