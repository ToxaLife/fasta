class Diagonal


  attr_accessor :start,
                :finish,
                :len,
                :score

  def initialize
    @score = 0
  end

  def to_s
    "(#{@start.x},#{@start.y}-#{@finish.x},#{@finish.y}) with len #{@len} and score #{@score}"
  end

end