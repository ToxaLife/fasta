require_relative 'database'
require_relative 'sequence'

SUBSMATRIX_FILEPATH = '../matrix/blosum62'
QUERYSEC_FILEPATH = '../input/queryseq.fasta'
DATABASESEC_FILEPATH = '../input/database.fasta'
LOGS_FILEPATH = '../output/logs.log'
KTUP_DEFAULT = 2
BESTDIAG_DEFAULT = 10
GAP = -4
WIDTH_DEFAULT = 2


Coords = Struct.new(:x, :y)
Alignment = Struct.new(:query_seq, :data_seq, :score, :query_alignment, :data_alignment)

$subs_matrix = {}

puts 'FastA alignment algorithm'

current_db = Database.new
current_db.open_seq_files(QUERYSEC_FILEPATH, DATABASESEC_FILEPATH)

query_seq = current_db.get_query
data_seq = current_db.get_data

if query_seq.length > 0
  puts "Found #{query_seq.length} query sequences"
else
  puts 'No query sequences found'
end

if data_seq.length > 0
  puts "Found #{data_seq.length} database sequences"
else
  puts 'No database sequences found'
end



current_db.set_parameters(LOGS_FILEPATH, KTUP_DEFAULT, BESTDIAG_DEFAULT, SUBSMATRIX_FILEPATH, GAP, WIDTH_DEFAULT)
current_db.do_fasta

puts 'Finished'
