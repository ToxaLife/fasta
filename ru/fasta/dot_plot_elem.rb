class DotPlotElem

  attr_accessor :x,
                :y,
                :point,
                :score,
                :is_diag,
                :region

  def initialize(x, y, point)
    @x = x
    @y = y
    @point = point
    @is_diag = false
    @score = 0
    @region = false
  end

  def print_point
    if @point
      if @is_diag
        print "▀".colorize(:red)
      else
        print "▀"
      end
    else
      if @is_diag
        print "×".colorize(:red)
      else
        print "×"
      end
    end
  end

  def print_score
    if @point
      if @is_diag
        print @score.to_s.colorize(:red)
      elsif @region
        print @score.to_s.colorize(:green)
      else
        print @score.to_s
      end
    else
      if @is_diag
        print @score.to_s.colorize(:blue)
      elsif @region
        print @score.to_s.colorize(:green)
      else
        print @score.to_s
      end
    end
  end
end