require_relative 'diagonal'
require_relative 'dot_plot_elem'
require 'colorize'



class Fasta

  def initialize(query, data)
    @query = query
    @data = data
  end

  def set_parameters(ktup, best_diag_count, gap, width)
    @ktup = ktup
    @best_diag_count = best_diag_count
    @gap = gap
    @width = width
    @result_score = 0
  end

  def algorithm
    build_dotplot
    detect_diags
    return '', '', 0 if @diag_list.empty?
    select_best_diags
    seq1, seq2 = find_best_seq
    return seq1, seq2, @result_score
  end

  private

  @dot_plot
  @diag_list
  @print_hash
  @matrix

  def build_dotplot
    @dot_plot = Array.new(@query.seq.length) {|i| Array.new(@data.seq.length) {|j| DotPlotElem.new(i, j, false)}}
    @data.seq.split('').each_with_index { |letter, i| @query.lookup_table[letter].each { |j| @dot_plot[j][i].point = true } if @query.lookup_table.has_key?(letter) }
  end

  def detect_diags
    @diag_list = Array.new
    m = @query.seq.length
    n = @data.seq.length
    (0...n + m - 1).each do |p|
      flag = false
      diag = Diagonal
      [p, m-1].min.downto([0, p-n+1].max).each do |q|
        x = m - 1 - q
        y = p - q
        if @dot_plot[x][y].point

          if not flag
            flag = true
            diag = Diagonal.new
            diag.start = Coords.new(x, y)
            diag.len = 1

          else
            diag.len += 1
            diag.finish = Coords.new(x, y)
          end

          if q == [0, p-n+1].max and flag and diag.len >= @ktup
            @diag_list.push(diag)
            # (0...diag.len).each { |i| @dot_plot[diag.start.x + i][diag.start.y + i ].is_diag = true }
            flag = false
          end
        else
          if flag and diag.len >= @ktup
            @diag_list.push(diag)
            # (0...diag.len).each { |i| @dot_plot[diag.start.x + i][diag.start.y + i].is_diag = true }
          end

          flag = false
        end
      end

    end
  end

  def select_best_diags

    @diag_list.sort! {|a, b| b.len <=> a.len }
    @diag_list.pop while @diag_list.length > @best_diag_count
    @diag_list.each { |diag| (diag.start.x..diag.finish.x).each_with_index { |i, index| diag.score += $subs_matrix[@query.seq[i]][@data.seq[diag.start.y + index]]}}



    @diag_list.each do |diag|
      up_extension = Array.new(1, diag.score)
      (1..[diag.start.x, diag.start.y].min).each do |index|
        if up_extension.last > 0
          up_extension.push(up_extension.last + $subs_matrix[@query.seq[diag.start.x - index]][@data.seq[diag.start.y - index]])
        else
          up_extension.pop
          break
        end
      end
      up_extension.reverse!
      diag.score = up_extension.max
      diag.start.x -= up_extension.length - up_extension.index(diag.score) - 1
      diag.start.y -= up_extension.length - up_extension.index(diag.score) - 1
      diag.len = diag.finish.x - diag.start.x
      # (0..diag.len).each { |i| @dot_plot[diag.start.x + i][diag.start.y + i].is_diag = true }
    end

    @diag_list.each do |diag|
      up_extension = Array.new(1, diag.score)
      (1...[@query.seq.length - diag.finish.x, @data.seq.length - diag.finish.y].min).each { |index| if up_extension.last > 0 then up_extension.push(up_extension.last + $subs_matrix[@query.seq[diag.finish.x + index]][@data.seq[diag.finish.y + index]]) else break end }
      up_extension.reverse!
      diag.score = up_extension.max
      diag.finish.x += up_extension.length - up_extension.index(diag.score) - 1
      diag.finish.y += up_extension.length - up_extension.index(diag.score) - 1
      diag.len = diag.finish.x - diag.start.x
      # (0..diag.len).each { |i| @dot_plot[diag.start.x + i][diag.start.y + i].is_diag = true }
    end
    @diag_list.uniq! { |elem| elem.start.x and elem.start.y}
    @diag_list.sort! { |a, b| b.score <=> a.score}




  end

  def find_best_seq
    best_diagonal = @diag_list.first
    up_border = [best_diagonal.start.x - @width, 0].max
    left_border = [best_diagonal.start.y - @width, 0].max
    bottom_border = [best_diagonal.finish.x + @width, @query.seq.length - 1].min
    right_border = [best_diagonal.finish.y + @width, @data.seq.length - 1].min
    left_diag = best_diagonal.start.x - best_diagonal.start.y - @width
    right_diag = best_diagonal.start.x - best_diagonal.start.y + @width
    max_elem = DotPlotElem

    (up_border..bottom_border).each do |i|
      (left_border..right_border).each do |j|
        if i - j < left_diag or i - j > right_diag
          next
        end
        @dot_plot[i][j].region = true
        diag_elem = @dot_plot[i-1][j-1]
        left_elem = @dot_plot[i][j-1]
        up_elem = @dot_plot[i-1][j]

        diag_score = (if diag_elem.nil? then 0 else diag_elem.score end) + $subs_matrix[@query.seq[i]][@data.seq[j]]
        left_score =  (if left_elem.nil? then 0 else left_elem.score end) + @gap
        up_score =  (if up_elem.nil? then 0 else up_elem.score end) + @gap

        @dot_plot[i][j].score = [diag_score, left_score, up_score, 0].max

        if @dot_plot[i][j].score > @result_score or max_elem.nil?
          max_elem = @dot_plot[i][j]
          @result_score = @dot_plot[i][j].score

        end
      end
    end
    seq1 = ''
    seq2 = ''


    cur_elem = max_elem

    while cur_elem.score > 0
      diag_elem = @dot_plot[cur_elem.x-1][cur_elem.y-1]
      left_elem = @dot_plot[cur_elem.x][cur_elem.y-1]
      up_elem = @dot_plot[cur_elem.x-1][cur_elem.y]

      diag_score = (if diag_elem.nil? then 0 else diag_elem.score end) + $subs_matrix[@query.seq[cur_elem.x]][@data.seq[cur_elem.y]]
      left_score =  (if left_elem.nil? then 0 else left_elem.score end) + @gap
      up_score =  (if up_elem.nil? then 0 else up_elem.score end) + @gap

      if cur_elem.score == diag_score
        seq1 += @query.seq[cur_elem.x]
        seq2 += @data.seq[cur_elem.y]
        cur_elem = @dot_plot[cur_elem.x-1][cur_elem.y-1]
      elsif cur_elem.score == left_score
        seq1 += @query.seq[cur_elem.x]
        seq2 += '-'
        cur_elem = @dot_plot[cur_elem.x][cur_elem.y-1]
      elsif cur_elem.score == up_score
        seq1 += '-'
        seq2 += @data.seq[cur_elem.y]
        cur_elem = @dot_plot[cur_elem.x-1][cur_elem.y]
      elsif cur_elem.x == 0 and cur_elem.y == 0
        seq1 += @query.seq[0]
        seq2 += @data.seq[0]
        break
      end
    end

    seq1.reverse!
    seq2.reverse!

    return seq1, seq2
  end

  def print_dot_plot
    print '  '
    @data.seq.split('').each { |letter| print "#{letter} " }
    puts

    @dot_plot.to_a.each_with_index { |row, i|
      print "#{@query.seq[i]} "
      row.each_with_index { |elem, j| print " #{elem.print_point}" }
      puts
    }
  end

  def print_score_plot
    print "\t"
    @data.seq.split('').each { |letter| print "#{letter}\t" }
    puts

    @dot_plot.to_a.each_with_index { |row, i|
      print "#{@query.seq[i]}\t"
      row.each_with_index { |elem, j| print "\t#{elem.print_score}" }
      puts
    }
  end
end

